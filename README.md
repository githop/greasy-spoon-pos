# Greasy Spoon POS

### Setup instructions

This project depends on create-react-app. Install if neccessary.

    npm install -g create-react-app

Install npm dependencies. In project root, run:

    npm install

The https://check-api.herokuapp.com API requires an access token.
Provide an access token by creating the following file:
src/lib/token.ts (to be imported in src/lib/api-client.ts)

    // token.ts
    export API_TOKEN = <api token here>;

Serve app in development mode:

    npm run start
    
### Requirements

[requirements](https://bitbucket.org/githop/greasy-spoon-pos/src/12036c2761e5cb58b829c8a0993bcaf5c22cc7b2/requirements.md?fileviewer=file-view-default)
[design notes](https://bitbucket.org/githop/greasy-spoon-pos/src/59eb9926f9f2995275ae8c60692b26aed07bafd2/design-notes.md?at=master&fileviewer=file-view-default)
