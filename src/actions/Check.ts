import { Action, Dispatch } from 'redux';
import { AveroApiClient } from '../lib/api-client';
import { ChecksActions } from './Checks';
import { CheckModel, OrderedItemModel } from '../models';
import { IChecksStore } from '../reducers';

export enum CheckActionTypes {
  AddToTable = '[Check] Add to table',
  AddToTableSuccess = '[Check] Add to table success',
  AddToTableError = '[Check] Add to table error',
  LoadCheckDetail = '[Check] Load check detail',
  LoadCheckDetailSuccess = '[Check] Load check detail success',
  LoadCheckDetailError = '[Check] Load check detail error',
  AddItem = '[Check] Add Item',
  AddItemSuccess = '[Check] Add item success',
  AddItemError = '[Check] Add item error',
  VoidItem = '[Check] Void item',
  VoidItemSuccess = '[Check] Void item success',
  VoidItemError = '[Check] Void item error',
  CloseCheck = '[Check] Close check',
  CloseCheckSuccess = '[Check] Close check success',
  CloseCheckError = '[Check] Close check error',
}

export class CheckAddToTable implements Action {
  readonly type = CheckActionTypes.AddToTable;
  get action() {
    return { type: this.type };
  }
}

export class CheckAddToTableSuccess implements Action {
  readonly type = CheckActionTypes.AddToTableSuccess;
  constructor(public payload: IChecksStore) {}
  get action() {
    return {
      type: this.type,
      payload: this.payload
    };
  }
}

export class CheckAddToTableError implements Action {
  readonly type = CheckActionTypes.AddToTableError;
  constructor(public payload: any) {}
  get action() {
    return {
      type: this.type,
      payload: this.payload
    };
  }
}

export class CheckDetailLoad implements Action {
  readonly type = CheckActionTypes.LoadCheckDetail;
  get action() {
    return { type: this.type };
  }
}

export class CheckDetailLoadSuccess implements Action {
  readonly type = CheckActionTypes.LoadCheckDetailSuccess;
  constructor(public payload: { [id: string]: CheckModel }) {}
  get action() {
    return { type: this.type, payload: this.payload };
  }
}

export class CheckDetailLoadError implements Action {
  readonly type = CheckActionTypes.LoadCheckDetailError;
  constructor(public payload: string) {}
  get action() {
    return { type: this.type, payload: this.payload };
  }
}

export class CheckAddItem implements Action {
  readonly type = CheckActionTypes.AddItem;
  get action() {
    return { type: this.type };
  }
}

export class CheckAddItemSuccess implements Action {
  readonly type = CheckActionTypes.AddItemSuccess;
  constructor(public payload: OrderedItemModel) {}
  get action() {
    return { type: this.type, payload: this.payload };
  }
}

export class CheckAddItemError implements Action {
  readonly type = CheckActionTypes.AddItemError;
  constructor(public payload: string) {}
  get action() {
    return { type: this.type, payload: this.payload };
  }
}

export class CheckVoidItem implements Action {
  readonly type = CheckActionTypes.VoidItem;
  get action() {
    return { type: this.type };
  }
}

export class CheckVoidItemSuccess implements Action {
  readonly type = CheckActionTypes.VoidItemSuccess;
  constructor(public payload: OrderedItemModel) {}
  get action() {
    return { type: this.type, payload: this.payload };
  }
}

export class CheckVoidItemError implements Action {
  readonly type = CheckActionTypes.VoidItemError;
  constructor(public payload: string) {}
  get action() {
    return { type: this.type, payload: this.payload };
  }
}

export class CloseCheck implements Action {
  readonly type = CheckActionTypes.CloseCheck;
  get action() {
    return { type: this.type };
  }
}

export class CloseCheckSuccess implements Action {
  readonly type = CheckActionTypes.CloseCheckSuccess;
  constructor(public payload: CheckModel) {}
  get action() {
    return { type: this.type, payload: this.payload };
  }
}

export class CloseCheckError implements Action {
  readonly type = CheckActionTypes.CloseCheckError;
  constructor(public payload: string) {}
  get action() {
    return { type: this.type, payload: this.payload };
  }
}

export type CheckActions = CheckAddToTable
    | CheckAddToTableError
    | CheckAddToTableSuccess
    | CheckDetailLoad
    | CheckDetailLoadSuccess
    | CheckDetailLoadError
    | CheckAddItem
    | CheckAddItemSuccess
    | CheckAddItemError
    | CheckVoidItem
    | CheckVoidItemError
    | CheckVoidItemSuccess
    | CloseCheck
    | CloseCheckSuccess
    | CloseCheckError;

export const AsyncCloseCheck = (checkId: string) => {
  return async (dispatch: Dispatch<CheckActions>) => {
    try {
      dispatch(new CloseCheck().action);
      const model = await AveroApiClient.closeCheck(checkId);
      const closeAction = new CloseCheckSuccess(model);
      dispatch(closeAction.action);
    } catch (e) {
      dispatch(new CloseCheckError(e).action);
    }
  };
};

export const AsyncVoidItemOnCheck = ({orderedItemId, checkId}: any) => {
  return async (dispatch: Dispatch<CheckActions>) => {
    try {
      dispatch(new CheckVoidItem().action);
      const voidedItem = await AveroApiClient.voidItemOnCheck(checkId, orderedItemId);
      const voidAction = new CheckVoidItemSuccess(voidedItem);
      dispatch(voidAction.action);
    } catch (e) {
      dispatch(new CheckVoidItemError(e).action);
    }
  };
};

export const AsyncAddItemToCheck = ({ checkId, itemId }: any) => {
  return async (dispatch: Dispatch<ChecksActions>) => {
    try {
      dispatch(new CheckAddItem().action);
      const orderedItem = await AveroApiClient.addItemToCheck(checkId, itemId);
      const orderedItemAction = new CheckAddItemSuccess(orderedItem);
      dispatch(orderedItemAction.action);
    } catch (e) {
      dispatch(new CheckAddItemError(e).action);
    }
  };
};

export const AsyncAddCheckToTable = (tableId: string) => {
  return async (dispatch: Dispatch<CheckActions>) => {
    try {
      dispatch(new CheckAddToTable().action);
      const newCheckDetail = await AveroApiClient.addCheckToTable(tableId);
      const checkDetailaction = new CheckAddToTableSuccess(newCheckDetail);
      dispatch(checkDetailaction.action);
    } catch (e) {
      dispatch(new CheckAddToTableError(e).action);
    }
  };
};

export const AsyncCheckLoad = (checkId: string) => {
  return async (dispatch: Dispatch<CheckActions>) => {
    try {
      dispatch(new CheckDetailLoad().action);
      const checkDetailModel = await AveroApiClient.getCheckDetail(checkId);
      const checkAction = new CheckDetailLoadSuccess(checkDetailModel);
      dispatch(checkAction.action);
    } catch (error) {
      dispatch(new CheckDetailLoadError(error).action);
    }
  };
};