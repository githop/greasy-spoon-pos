import { Action } from 'redux';
import { CheckStatuses } from '../reducers';

export enum FiltersActionTypes {
  SelectTable = '[Filters] Select table',
  SelectCheckStatus = '[Filters] Select check status'
}

export class FiltersSelectCheckStatus implements Action {
  readonly type = FiltersActionTypes.SelectCheckStatus;
  constructor(public payload: CheckStatuses) {}
  get action() {
    return { type: this.type, payload: this.payload };
  }
}

export class FiltersSelectTable implements Action {
  readonly type = FiltersActionTypes.SelectTable;
  constructor(public payload: string) {}
  get action() {
    return { type: this.type, payload: this.payload };
  }
}

export type FiltersActions = | FiltersSelectCheckStatus | FiltersSelectTable;
