import { Action, Dispatch } from 'redux';
import { AveroApiClient } from '../lib/api-client';
import { ITablesStore } from '../reducers';

export enum TablesActionTypes {
  AsyncLoad = '[Tables] Load',
  LoadSuccess = '[Tables] Load Success',
  LoadError = '[Tables] Load Error'
}

export class TablesLoadSuccess implements Action {
  readonly type = TablesActionTypes.LoadSuccess;
  constructor(public payload: ITablesStore) {}
  get action() {
    // because actions must be plain objects
    return { type: this.type, payload: this.payload };
  }
}

export class TablesLoadError implements Action {
  readonly type = TablesActionTypes.LoadError;
  constructor(public payload: any) {}
  get action() {
    // because actions must be plain objects
    return { type: this.type, payload: this.payload };
  }
}

export class TablesLoad implements Action {
  readonly type = TablesActionTypes.AsyncLoad;
  get action() {
    // because actions must be plain objects
    return { type: this.type };
  }
}

export const AsyncTablesLoad = () => {
  return (dispatch: Dispatch<TablesActions>) => {
    dispatch(new TablesLoad().action);
    AveroApiClient.getTablesIndex()
        .then(tablesStore => new TablesLoadSuccess(tablesStore))
        .then(tableAction => dispatch(tableAction.action))
        .catch(error => dispatch(new TablesLoadError(error).action));
  };
};

export type TablesActions = TablesLoad | TablesLoadSuccess | TablesLoadError;