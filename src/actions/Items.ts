import { Action, Dispatch } from 'redux';
import { IItemsStore } from '../reducers';
import { AveroApiClient } from '../lib/api-client';

export enum ItemActionTypes {
  Load = '[Items] Load',
  LoadSuccess = '[Items] Load success',
  LoadError = '[Items] Load error',
}

export class ItemsLoad implements Action {
  readonly type = ItemActionTypes.Load;
  get action() {
    return { type: this.type };
  }
}

export class ItemsLoadSuccess implements Action {
  readonly type = ItemActionTypes.LoadSuccess;
  constructor(public payload: IItemsStore) {}
  get action() {
    return { type: this.type, payload: this.payload };
  }
}

export class ItemsLoadError implements Action {
  readonly type = ItemActionTypes.LoadError;
  constructor(public payload: string) {}
  get action() {
    return { type: this.type, payload: this.payload };
  }
}

export type ItemsActions = ItemsLoad | ItemsLoadError | ItemsLoadSuccess;

export const AsyncItemsLoad = () => {
  return (dispatch: Dispatch<ItemsActions>) => {
    dispatch(new ItemsLoad().action);
    AveroApiClient.getItemsIndex()
        .then(itemsStore => new ItemsLoadSuccess(itemsStore))
        .then(itemsAction => dispatch(itemsAction.action))
        .catch(e => dispatch(new ItemsLoadError(e).action));
  };
};