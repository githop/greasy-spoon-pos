import { Action, Dispatch } from 'redux';
import { AveroApiClient } from '../lib/api-client';

export enum ChecksActionTypes {
  AsyncLoad = '[Checks] Load',
  LoadSuccess = '[Checks] Load success',
  LoadError = '[Checks] Load error'
}

export class ChecksLoadSuccess implements Action {
  readonly type = ChecksActionTypes.LoadSuccess;
  constructor(public payload: any) {}
  get action() {
    return {
      type: this.type,
      payload: this.payload
    };
  }
}

export class ChecksLoadError implements Action {
  readonly type = ChecksActionTypes.LoadError;
  constructor(public payload: string) {}
  get action() {
    return {
      type: this.type,
      payload: this.payload
    };
  }
}

export class ChecksLoad implements Action {
  readonly type = ChecksActionTypes.AsyncLoad;
  get action() {
    return { type: this.type };
  }
}

export type ChecksActions = ChecksLoad
    | ChecksLoadSuccess
    | ChecksLoadError;

export const AsyncChecksLoad = () => {
  return async (dispatch: Dispatch<ChecksActions>) => {
    try {
      dispatch(new ChecksLoad().action);
      const checksStore = await AveroApiClient.getChecksIndex();
      const checksAction = new ChecksLoadSuccess(checksStore);
      dispatch(checksAction.action);
    } catch (error) {
      dispatch(new ChecksLoadError(error).action);
    }
  };
};
