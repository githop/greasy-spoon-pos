import * as React from 'react';
import './ChecksIndex.css';
import { ITablesStore, IChecksStore, CheckStatuses, emptyChecksState } from '../../reducers';
import { Link } from 'react-router-dom';
import { TableSelect } from '../TableSelect';
import { TableRadios } from '../TableRadios/TableRadios';
import { FiltersSelectCheckStatus, FiltersSelectTable } from '../../actions';

interface Props {
  tables: ITablesStore;
  checks: IChecksStore;
  selectedTable: string;
  selectedCheckStatus: CheckStatuses;
  addCheckToTable: (tableId: string) => Promise<void>;
  onTableSelect: (tableId: string) => FiltersSelectTable;
  onCheckStatusSelect: (checkStatus: CheckStatuses) => FiltersSelectCheckStatus;
}

export const ChecksIndex: React.StatelessComponent<Props> = (props: Props) => {
  const {
    tables,
    checks,
    selectedTable,
    selectedCheckStatus,
    addCheckToTable,
    onTableSelect,
    onCheckStatusSelect
  } = props;

  const styleStatusText = (statusClosed: boolean) => ({
    color: statusClosed ? '#C9CF31' : '#D62A1C'
  });

  const renderChecks = () => {
    return Object.keys(checks).map((checkId: string) => {
      const check = checks[checkId];
      const table = tables[check.tableId];
      if (table == null) {
        return null;
      }
      return (
          <tr key={checkId}>
            <td>{table.number}</td>
            <td style={styleStatusText(check.closed)}>{check.closed ? 'closed' : 'open'}</td>
            <td><Link className="button button-black button-outline" to={`/checks/${checkId}`}>details</Link></td>
          </tr>
      );
    });
  };

  const renderAddCheckToTableButton = () => {
    const currentTable = tables[selectedTable];
    if (currentTable == null) {
      return null;
    }
    return (
        <button
            className="button button-black button-outline"
            disabled={currentTable.hasOpenCheck}
            onClick={() => addCheckToTable(currentTable.id)}
        >
          Add check to table
        </button>
    );
  };

  const renderChecksTable = () => {
    const currentTable = tables[selectedTable];
    if (emptyChecksState(checks)) {
      if (currentTable) {
        return (
            <div>
              <h2>No {selectedCheckStatus !== 'all' && selectedCheckStatus} checks for table {currentTable.number}</h2>
            </div>
        );
      }
      return (
          <div>
            <h2>No {selectedCheckStatus !== 'all' && selectedCheckStatus} checks on any table.</h2>
          </div>
      );
    }

    return (
        <table>
          <thead>
          <tr>
            <th>Table Number</th>
            <th>Check Status</th>
            <th>Details</th>
          </tr>
          </thead>
          <tbody>{renderChecks()}</tbody>
        </table>
    );
  };

  return (
      <div className="checks">
        <section className="controls">
          <TableSelect
              tables={tables}
              selectedTable={selectedTable}
              onTableSelect={(tableId: string) => onTableSelect(tableId)}
          />
          <TableRadios
              selectedCheckStatus={selectedCheckStatus}
              onCheckStatusSelect={(status: CheckStatuses) => onCheckStatusSelect(status)}
          />
        </section>
        {renderAddCheckToTableButton()}
        <section className="checks-table">
          {renderChecksTable()}
        </section>
      </div>
  );
};
