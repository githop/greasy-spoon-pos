import * as React from 'react';
import './TableRadios.css';
import { CheckStatuses } from '../../reducers';
import { FiltersSelectCheckStatus } from '../../actions';
interface Props {
  selectedCheckStatus: string;
  onCheckStatusSelect: (checkStatus: CheckStatuses) => FiltersSelectCheckStatus;
}

export const TableRadios: React.StatelessComponent<Props> = (props: Props) => {
  const { selectedCheckStatus, onCheckStatusSelect }  = props;
  const renderRadioRow = (status: CheckStatuses) => {
    return (
        <label key={status}>{status}
          <input
              type="radio"
              value={status}
              checked={status === selectedCheckStatus}
              onChange={() => onCheckStatusSelect(status)}
          />
        </label>
    );
  };

  return (
      <div>
        <label>Filter by Check Status</label>
        <section className="radios">
          {['all', 'closed', 'open'].map((status: CheckStatuses) => renderRadioRow(status))}
        </section>
      </div>
  );
};