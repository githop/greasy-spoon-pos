import * as React from 'react';
import './TablesIndex.css';
import { ITablesStore } from '../../reducers';
import { Link } from 'react-router-dom';
import { FiltersSelectTable } from '../../actions';

interface Props {
  tables: ITablesStore;
  selectTable: (tableId: string) => FiltersSelectTable;
}

export const TablesIndex: React.StatelessComponent<Props> = (props: Props) => {
  const { tables, selectTable, } = props;

  const tableClick = (tableId: string) => {
    selectTable(tableId);
  };

  const setStatusClass = (hasOpenCheck: boolean) => {
    return hasOpenCheck ? 'table--occupied' : 'table--unoccupied';
  };

  const renderTables = (t: ITablesStore) => {
    return Object.keys(t).map(key => {
      const table = t[key];
      return (
          <div className={`table ${setStatusClass(!!table.hasOpenCheck)}`} key={key}>
            <h1>{table.number}</h1>
            <p>Table {!!table.hasOpenCheck ? 'occupied' : 'unoccupied'}</p>
            <Link
                className="button button-black button-outline"
                to="/checks"
                onClick={() => tableClick(key)}>
              details
            </Link>
          </div>
      );
    });
  };

  return (
      <div className="tables">
        <h4>Tables</h4>
        <div className="tables-wrapper">
          {renderTables(tables)}
        </div>
      </div>
  );
};