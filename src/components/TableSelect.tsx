import * as React from 'react';
import { ITablesStore } from '../reducers';
import { FiltersSelectTable } from '../actions';

interface Props {
  tables: ITablesStore;
  selectedTable: string;
  onTableSelect: (tableId: string) => FiltersSelectTable;
}

export class TableSelect extends React.Component<Props> {

  render() {
    return (
        <label>Filter by Table
          <select value={this.props.selectedTable} onChange={(e) => this.handleChange(e)}>
            <option value="all">All</option>
            {this.renderSelectOptions(this.props.tables)}
          </select>
        </label>
    );
  }

  private handleChange(e: any) {
    const tableId = e.target.value;
    this.props.onTableSelect(tableId);
  }

  private renderSelectOptions(tables: ITablesStore) {
    return Object.keys(tables).map(key => {
      return <option key={key} value={key}>{tables[key].number}</option>;
    });
  }
}
