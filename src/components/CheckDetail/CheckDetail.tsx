import * as React from 'react';
import { CheckModel, OrderedItemModel } from '../../models';
import { emptyItemsState, IItemsStore } from '../../reducers';
import './CheckDetail.css';

interface Props {
  checkDetail: CheckModel;
  items: IItemsStore;
  addItemToCheck: (d: { itemId: string, checkId: string }) => Promise<void>;
  voidItemOnCheck: (d: { orderedItemId: string, checkId: string }) => Promise<void>;
  closeCheck: (checkId: string) => Promise<void>;
}

export const CheckDetail: React.StatelessComponent<Props> = (props) => {
  const {checkDetail, items, addItemToCheck, voidItemOnCheck, closeCheck} = props;

  const dispatchAddItem = (itemId: string) => {
    addItemToCheck({itemId, checkId: checkDetail.id});
  };

  const dispatchVoidItem = (orderedItemId: string) => {
    voidItemOnCheck({orderedItemId, checkId: checkDetail.id});
  };

  const dispatchCloseCheck = (checkId: string) => {
    closeCheck(checkId);
  };

  const renderMenuItems = () => {
    return Object.keys(items).map(itemKey => {
      const item = items[itemKey];
      return (
          <div className="menu-item" key={itemKey}>
            <div>
              <h6>{item.name}</h6>
              <small>${item.price}</small>
            </div>
            <button
                className="button button-black button-outline"
                onClick={() => dispatchAddItem(item.id)}
            >
              add to order
            </button>
          </div>
      );
    });
  };

  const renderOrderedItems = () => {
    if (checkDetail.orderedItems === null) {
      return (
          <div>
            <h1>No orders on check</h1>.
          </div>
      );
    }

    return (
        <table>
          <thead>
          <tr>
            <th>Total: {calcTotalPrice()}</th>
            <th>Item</th>
            <th>Voided</th>
          </tr>
          </thead>
          <tbody>{renderOrderedItemsTableBody()}</tbody>
        </table>
    );
  };

  const renderOrderedItemsTableBody = () => {
    return checkDetail.orderedItems.map(oi => {
      const item = items[oi.itemId];
      if (item == null) {
        return null;
      }
      const allowVoidContents = (
          <button
              disabled={oi.voided}
              onClick={() => dispatchVoidItem(oi.id)}
              className="button button-black button-outline"
          >
            void item
          </button>
      );

      return (
          <tr key={oi.id} style={{textDecoration: oi.voided && 'line-through'}}>
            <td>{item.price}</td>
            <td>{item.name}</td>
            <td>{oi.voided ? 'voided' : !checkDetail.closed && allowVoidContents}</td>
          </tr>
      );
    });
  };

  const calcTotalPrice = () => {
    if (checkDetail.orderedItems.length > 0 && !emptyItemsState(items)) {
      return checkDetail.orderedItems.reduce(
          (total: number, orderedItem: OrderedItemModel) => {
            const item = items[orderedItem.itemId];
            if (!orderedItem.voided) {
              total += item.price;
            }
            return total;
          },
          0
      );
    }
    return 0;
  };

  const renderMenuItemsSection = () => {
    if (checkDetail.closed) {
      return null;
    }
    return (
        <div className="check-panel check-menu">
          {renderMenuItems()}
        </div>
    );
  };

  const renderOrderedItemsSection = () => {
    return (
        <div className="check-panel">
          {renderTaxAndTip()}
          {renderOrderedItems()}
        </div>
    );
  };

  const renderCloseCheckButton = () => {
    if (checkDetail.closed) {
      return null;
    }
    return (
        <button
            className="button button-black button-outline"
            disabled={checkDetail.orderedItems.length === 0}
            onClick={() => {
              dispatchCloseCheck(checkDetail.id);
            }}
        >
          close check
        </button>
    );
  };

  const renderTaxAndTip = () => {
    if (!checkDetail.closed) {
      return null;
    }
    return (
        <blockquote className="tax-and-tips">
          <h4>Tax <small>{checkDetail.tax}</small></h4>
          <h4>Tip <small>{checkDetail.tip}</small></h4>
        </blockquote>
    );
  };

  return (
      <div>
        {renderCloseCheckButton()}
        <div className="check-details">
          {renderMenuItemsSection()}
          {renderOrderedItemsSection()}
        </div>
      </div>
  );
};
