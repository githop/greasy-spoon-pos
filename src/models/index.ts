
export class TableModel {
  id: string;
  number: number;
  hasOpenCheck?: boolean;  // client side only
}

export class ItemModel {
  id: string;
  name: string;
  price: number;
}

export class CheckModel {
  id: string;
  dateCreated: Date;
  dateUpdated: Date;
  createdBy: string;
  tableId: string;
  closed: boolean;
  tax: number;
  tip: number;
  orderedItems: OrderedItemModel[] = [];
}

export class OrderedItemModel {
  id: string;
  dateCreated: Date;
  dateUpdated: Date;
  createdBy: string;
  checkId: string;
  itemId: string;
  voided: boolean;
}

export const createInstance = <T>(Klass: new () => T, data: any): T => {
  return Object.assign(new Klass(), data);
};