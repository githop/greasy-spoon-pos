import { IAppState } from '../reducers';
import { IChecksStore } from '../reducers';
import { createSelector } from 'reselect';
import { CheckModel } from '../models';

export const getChecks = (state: IAppState): IChecksStore => {
  return state.checks;
};

export const getChecksAsArray = (state: IAppState): CheckModel[] => {
  return Object.keys(state.checks).map(k => state.checks[k]);
};

const getSelectedTable = (state: IAppState) => state.filters.selectedTable;
const getCheckFilterStatus = (state: IAppState) => state.filters.status;

export const makefilterChecksByTable = () => {
  return createSelector(
      [getChecks, getSelectedTable, getCheckFilterStatus],
      (checks: IChecksStore, selectedTable: string, checkStatus: string) => {
        // no filters applied
        if (selectedTable === 'all' && checkStatus === 'all') {
          return checks;
        }
        return Object.keys(checks).reduce(
            (map: any, checkId: string) => {
              const check = checks[checkId];
              // filter by selected table
              if (selectedTable !== 'all') {
                // filter selected table by open closed
                if (check.tableId === selectedTable) {
                  switch (checkStatus) {
                    case 'all':
                      map[checkId] = check;
                      break;
                    case 'open':
                      if (!check.closed) {
                        map[checkId] = check;
                      }
                      break;
                    case 'closed':
                      if (check.closed) {
                        map[checkId] = check;
                      }
                      break;
                  }
                }
              } else {
                switch (checkStatus) {
                  case 'all':
                    map[checkId] = check;
                    break;
                  case 'open':
                    if (!check.closed) {
                      map[checkId] = check;
                    }
                    break;
                  case 'closed':
                    if (check.closed) {
                      map[checkId] = check;
                    }
                    break;
                }
              }
              return map;
            },
            {}
        );
      }
  );
};