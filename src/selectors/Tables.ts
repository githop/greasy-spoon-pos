import { IAppState, ITablesStore } from '../reducers';
import { getChecksAsArray } from './Checks';
import { createSelector } from 'reselect';
import { CheckModel } from '../models';

const getTables = (state: IAppState) => state.tables;
export const makeCalcTablesStatus = () => {
  return createSelector(
      [getTables, getChecksAsArray],
      (tables: ITablesStore, checks: CheckModel[]) => {
        Object.keys(tables).forEach((tableKey) => {
          const table = tables[tableKey];
          const byTable = checks.filter(c => c.tableId === tableKey)
              .filter(c => !c.closed);
          table.hasOpenCheck = (byTable.length > 0);
        });
        return tables;
      }
  );
};