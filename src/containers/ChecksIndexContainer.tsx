import * as React from 'react';
import { IAppState, CheckStatuses, ITablesStore, IChecksStore, emptyChecksState, emptyTablesState } from '../reducers';
import { Dispatch } from 'redux';
import {
  AsyncChecksLoad,
  AsyncTablesLoad,
  FiltersSelectCheckStatus,
  FiltersSelectTable,
  AsyncAddCheckToTable
} from '../actions';
import { connect } from 'react-redux';
import { ChecksIndex } from '../components/ChecksIndex/ChecksIndex';
import { makefilterChecksByTable, makeCalcTablesStatus } from '../selectors';

class ChecksIndexContainer extends React.Component<Props, {}> {
  componentDidMount() {
    const singleCheck = Object.keys(this.props.checks).length === 1;
    const emptyTables = emptyTablesState(this.props.tables);
    // fetch checks index if checks store is empty or the only check in the
    // store is from the user coming from the detail page to the index page
    if (emptyChecksState(this.props.checks) || (singleCheck && emptyTables)) {
      this.props.fetchChecks();
    }
    if (emptyTables) {
      this.props.fetchTables();
    }
  }

  render() {
    if (this.props.checks == null || this.props.tables == null) {
      return <h1>Loading...</h1>;
    }

    return (
        <ChecksIndex
            tables={this.props.tables}
            checks={this.props.checks}
            selectedTable={this.props.selectedTable}
            selectedCheckStatus={this.props.selectedCheckStatus}
            onCheckStatusSelect={(status) => this.props.onCheckStatusSelect(status)}
            onTableSelect={(tableId) => this.props.onTableSelect(tableId)}
            addCheckToTable={(tableId) => this.props.addCheckToTable(tableId)}
        />
    );
  }
}

interface Props {
  tables: ITablesStore;
  checks: IChecksStore;
  selectedTable: string;
  selectedCheckStatus: CheckStatuses;
  fetchChecks: () => Promise<void>;
  fetchTables: () => Promise<void>;
  addCheckToTable: (tableId: string) => Promise<void>;
  onTableSelect: (tableId: string) => FiltersSelectTable;
  onCheckStatusSelect: (checkStatus: CheckStatuses) => FiltersSelectCheckStatus;
}

const makeMapStateToProps = () => {
  const calcTablesStatus = makeCalcTablesStatus();
  const filterChecksByTable = makefilterChecksByTable();
  const mapStateToProps = (state: IAppState) => ({
    checks: filterChecksByTable(state),
    tables: calcTablesStatus(state),
    selectedTable: state.filters.selectedTable,
    selectedCheckStatus: state.filters.status
  });

  return mapStateToProps;
};

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
  fetchChecks: () => dispatch(AsyncChecksLoad()),
  fetchTables: () => dispatch(AsyncTablesLoad()),
  addCheckToTable: (tableId: string) => dispatch(AsyncAddCheckToTable(tableId)),
  onTableSelect: (tableId: string) => dispatch(new FiltersSelectTable(tableId).action),
  onCheckStatusSelect: (status: CheckStatuses) => dispatch(new FiltersSelectCheckStatus(status).action)
});

export default connect(
    makeMapStateToProps,
    mapDispatchToProps
)(ChecksIndexContainer);