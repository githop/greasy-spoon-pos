import * as React from 'react';
import { Dispatch } from 'redux';
import { AsyncChecksLoad, FiltersSelectTable, AsyncTablesLoad, TablesActions } from '../actions';
import { connect } from 'react-redux';
import { TablesIndex } from '../components/TablesIndex/TablesIndex';
import { makeCalcTablesStatus } from '../selectors';
import { IAppState, IChecksStore, ITablesStore, emptyTablesState, emptyChecksState } from '../reducers';

class TablesIndexContainer extends React.Component<Props> {

  componentDidMount() {
    if (emptyChecksState(this.props.checks)) {
      this.props.fetchChecks();
    }
    if (emptyTablesState(this.props.tables)) {
      this.props.fetchTables();
    }
  }

  render() {
    if (emptyTablesState(this.props.tables)) {
      return <h3>Loading tables...</h3>;
    }

    return (
        <TablesIndex
            tables={this.props.tables}
            selectTable={(tableId: string) => this.props.selectTable(tableId)}
        />
    );
  }
}

interface Props {
  tables: ITablesStore;
  checks: IChecksStore;
  fetchChecks: () => Promise<void>;
  fetchTables: () => Promise<void>;
  selectTable: (tableId: string) => FiltersSelectTable;
}

const makeMapStateToProps = () => {
  const calcTablesStatus = makeCalcTablesStatus();
  const mapStateToProps = (state: IAppState) => ({
    tables: calcTablesStatus(state),
    checks: state.checks
  });

  return mapStateToProps;
};

const mapDispatchToProps = (dispatch: Dispatch<TablesActions>) => ({
  fetchChecks: () => dispatch(AsyncChecksLoad()),
  fetchTables: () => dispatch(AsyncTablesLoad()),
  selectTable: (tableId: string) => dispatch(new FiltersSelectTable(tableId).action)
});

export default connect(
    makeMapStateToProps,
    mapDispatchToProps
)(TablesIndexContainer);