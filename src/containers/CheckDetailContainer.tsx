import * as React from 'react';
import { IAppState, IItemsStore, emptyItemsState } from '../reducers';
import { Dispatch } from 'redux';
import {
  AsyncAddItemToCheck,
  AsyncCheckLoad,
  AsyncCloseCheck,
  AsyncItemsLoad,
  AsyncVoidItemOnCheck,
  CheckActions
} from '../actions';
import { connect } from 'react-redux';
import { CheckDetail } from '../components/CheckDetail/CheckDetail';
import { RouteComponentProps } from 'react-router';
import { CheckModel } from '../models';

interface Props extends RouteComponentProps<{ id: string }> {
  checkDetail: CheckModel;
  items: IItemsStore;
  fetchCheckDetail: (checkId: string) => Promise<void>;
  fetchItems: () => void;
  addItemToCheck: (d: {itemId: string, checkId: string}) => Promise<void>;
  voidItemOnCheck: (d: { orderedItemId: string, checkId: string }) => Promise<void>;
  closeCheck: (checkId: string) => Promise<void>;
}

class CheckDetailContainer extends React.Component<Props> {

  componentDidMount() {
    if (emptyItemsState(this.props.items)) {
      this.props.fetchItems();
    }

    if (this.props.checkDetail == null || this.props.checkDetail.orderedItems.length === 0) {
      this.props.fetchCheckDetail(this.props.match.params.id);
    }
  }

  render() {

    if (this.props.checkDetail == null || this.props.items == null) {
      return <h1>Loading...</h1>;
    }

    return (
        <CheckDetail
            checkDetail={this.props.checkDetail}
            items={this.props.items}
            addItemToCheck={(d: any) => this.props.addItemToCheck(d)}
            voidItemOnCheck={(d) => this.props.voidItemOnCheck(d)}
            closeCheck={(checkId) => this.props.closeCheck(checkId)}
        />
    );
  }
}

const mapStateToProps = (state: IAppState, props: any) => {
  const { match: { params: { id } } } = props;
  return {
    checkDetail: state.checks[id],
    items: state.items
  };
};

const mapDispatchToProps = (dispatch: Dispatch<CheckActions>) => ({
  fetchCheckDetail: (checkId: string) => dispatch(AsyncCheckLoad(checkId)),
  fetchItems: () => dispatch(AsyncItemsLoad()),
  addItemToCheck: (d: {itemId: string, checkId: string}) => dispatch(AsyncAddItemToCheck(d)),
  voidItemOnCheck: (d: {orderedItemId: string, checkId: string}) => dispatch(AsyncVoidItemOnCheck(d)),
  closeCheck: (checkId: string) => dispatch(AsyncCloseCheck(checkId))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CheckDetailContainer);