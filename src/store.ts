import { applyMiddleware, compose, createStore, Store } from 'redux';
import { IAppState, appState } from './reducers';
import reduxThunk from 'redux-thunk';

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store: Store<IAppState> = createStore(
    appState,
    composeEnhancers(
        applyMiddleware(reduxThunk)
    ),
);