import {
  CheckActions,
  TablesActions,
  ChecksActions,
  ChecksActionTypes,
  TablesActionTypes,
  CheckActionTypes, ItemActionTypes, ItemsActions
} from '../actions';

export interface IErrorsState {
  errors: string;
}

export const initialErrorsState = {
  errors: ''
};

export type AppActions = CheckActions | TablesActions | ChecksActions | ItemsActions;
export const errorsReducer = (state = initialErrorsState, action: AppActions): IErrorsState => {
  switch (action.type) {
    case ChecksActionTypes.LoadError:
    case TablesActionTypes.LoadError:
    case CheckActionTypes.AddToTableError:
    case ItemActionTypes.LoadError:
    case CheckActionTypes.AddItemError:
    case CheckActionTypes.VoidItemError:
    case CheckActionTypes.CloseCheckError:
      return action.payload;
    default:
      return state;
  }
};