export * from './Checks';
export * from './Errors';
export * from './Filters';
export * from './Items';
export * from './Tables';
import { combineReducers } from 'redux';
import { ITablesStore, tablesReducer } from './Tables';
import { IChecksStore, checksReducer } from './Checks';
import { errorsReducer } from './Errors';
import { IItemsStore, itemsReducer } from './Items';
import { filtersReducer, IFiltersState } from './Filters';

export interface IAppState {
  tables: ITablesStore;
  checks: IChecksStore;
  items: IItemsStore;
  errors: string;
  filters: IFiltersState;
}

export const appState = combineReducers<IAppState>({
  tables: tablesReducer,
  checks: checksReducer,
  items: itemsReducer,
  errors: errorsReducer,
  filters: filtersReducer
});