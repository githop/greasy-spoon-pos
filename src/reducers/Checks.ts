import { CheckModel } from '../models';
import { CheckActionTypes, ChecksActions, ChecksActionTypes, CheckActions } from '../actions';
import * as AppActions from '../actions';

export interface IChecksStore {
  [id: string]: CheckModel;
}

export interface IChecksState {
  checks: IChecksStore;
}

export const initialChecksState = {} as IChecksState;

export const checksReducer =
    (state: IChecksState = initialChecksState, action: ChecksActions | CheckActions): IChecksState => {
  switch (action.type) {
    case ChecksActionTypes.LoadSuccess:
      return handleChecksIndex(state, action);
    case CheckActionTypes.LoadCheckDetailSuccess:
    case CheckActionTypes.AddToTableSuccess:
      return {
        ...state,
        ...action.payload
      };
    case CheckActionTypes.AddItemSuccess:
      return addOrderedItem(state, action);
    case CheckActionTypes.VoidItemSuccess:
      return updateVoidedItem(state, action);
    case CheckActionTypes.CloseCheckSuccess:
      return closeCheck(state, action);
    default:
      return state;
  }
};

export const emptyChecksState = (checks: IChecksStore) => {
  return Object.keys(checks).length === 0;
};

function handleChecksIndex(state: IChecksState, action: AppActions.ChecksLoadSuccess) {
  const inStore = state[action.payload.id];
  if (inStore && inStore.orderedItems.length > 0) {
    action.payload.orderedItems = inStore.orderedItems;
  }
  return {
    ...state,
    ...action.payload
  };
}

function closeCheck(state: IChecksState, action: AppActions.CloseCheckSuccess) {
  state[action.payload.id] = action.payload;
  return {...state};
}

function addOrderedItem(state: IChecksState, action: AppActions.CheckAddItemSuccess) {
  const checkDetail = state[action.payload.checkId];
  checkDetail.orderedItems.push(action.payload);
  state[action.payload.checkId] = Object.assign({}, checkDetail);
  return {...state};
}

function updateVoidedItem(state: IChecksState, action: AppActions.CheckVoidItemSuccess) {
  const checkDetail = state[action.payload.checkId];
  for (const order of checkDetail.orderedItems) {
    if (order.id === action.payload.id) {
      order.voided = true;
      break;
    }
  }
  state[action.payload.checkId] = Object.assign({}, checkDetail);
  return {...state};
}