import { ItemModel } from '../models';
import { ItemActionTypes, ItemsActions } from '../actions';

export interface IItemsStore {
  [id: string]: ItemModel;
}

export interface IItemsState {
  items: IItemsStore;
}

const initialItemsState = {} as IItemsState;

export const itemsReducer = (state = initialItemsState, action: ItemsActions) => {
  switch (action.type) {
    case ItemActionTypes.LoadSuccess:
      return action.payload;
    default:
      return state;
  }
};

export const emptyItemsState = (state: IItemsStore) => {
  return Object.keys(state).length === 0;
};