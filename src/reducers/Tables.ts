import { TableModel } from '../models';
import { CloseCheckSuccess, TablesActions, TablesActionTypes, CheckActionTypes } from '../actions';

export interface ITablesStore {
  [id: string]: TableModel;
}

export interface ITablesState {
  tables: ITablesStore;
}

const initialTablesState = {} as ITablesState;
export const tablesReducer = (state = initialTablesState, action: TablesActions | CloseCheckSuccess): ITablesState => {
  switch (action.type) {
    case TablesActionTypes.LoadSuccess:
      return { ...state, ...action.payload };
    case CheckActionTypes.CloseCheckSuccess:
      return { ...state };
    default:
      return state;
  }
};

export const emptyTablesState = (tables: ITablesStore) => {
  return Object.keys(tables).length === 0;
};