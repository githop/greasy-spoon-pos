import { FiltersActions, FiltersActionTypes } from '../actions';

export type CheckStatuses = 'all' | 'open' | 'closed';
export interface IFiltersState {
  selectedTable: string;
  status: CheckStatuses;
}

export const initialFilterState = {
  selectedTable: 'all',
  status: 'all'
} as IFiltersState;

export const filtersReducer = (state = initialFilterState, action: FiltersActions): IFiltersState => {
  switch (action.type) {
    case FiltersActionTypes.SelectTable:
      state.selectedTable = action.payload;
      return { ...state };
    case FiltersActionTypes.SelectCheckStatus:
      state.status = action.payload;
      return { ...state };
    default:
      return state;
  }
};