
/*
  I typically would store an API token in an environment variable,
  although for the sake of brevity, using ES modules to export
  a string from an ignored file (from VCS) should suffice.

  for example:
  in lib/src/token.ts
  ---
  export const API_TOKEN = <api token here>;

  in .gitignore
  ---
  /lib/src/token.ts
*/
import { API_TOKEN } from './token';
import { CheckModel, createInstance, ItemModel, OrderedItemModel, TableModel } from '../models';
import { ITablesStore } from '../reducers/Tables';
import { IItemsStore } from '../reducers/Items';
import { IChecksStore } from '../reducers/Checks';
const API_BASE_URL = 'https://check-api.herokuapp.com';

type methods = 'GET' | 'POST' | 'PUT' | 'DELETE';
const authenticatedRequest = <T>(path: string, method: methods = 'GET', payload?: any): Promise<T> => {
  const headers = {
    'Authorization': API_TOKEN,
    'Content-Type': 'application/json'
  };
  const config = {
    method,
    headers
  };

  if (method === 'POST' || method === 'PUT') {
    // add payload, headers
    Object.assign(
        config,
        {
          body: JSON.stringify(payload)
        },
        {
          headers: {
            ...headers,
            'Accept': 'application/json',
          }
        }
    );
  }

  return fetch(API_BASE_URL + path, config)
      .then(response => response.json())
      .then((data) => {
        if (data.StatusCode >= 400) {
          return Promise.reject(data.Description);
        }
        return data;
      });
};

export class AveroApiClient {

  static getTablesIndex(): Promise<ITablesStore> {
    return authenticatedRequest('/tables')
        .then((data) => this.mapResponseToModel(TableModel, data))
        .then(models => this.mapModelsToStore(models));
  }

  static getItemsIndex(): Promise<IItemsStore> {
    return authenticatedRequest('/items')
        .then((data) => this.mapResponseToModel(ItemModel, data))
        .then(models => this.mapModelsToStore(models));
  }

  static getChecksIndex(): Promise<IChecksStore> {
    return authenticatedRequest('/checks')
        .then((data) => this.mapResponseToModel(CheckModel, data))
        .then(models => this.mapModelsToStore(models));
  }

  static addCheckToTable(tableId: string): Promise<IChecksStore> {
    return authenticatedRequest(`/checks`, 'POST', { tableId })
        .then(response => createInstance(CheckModel, response))
        .then(model => ({[model.id]: model}));
  }

  static getCheckDetail(checkId: string): Promise<IChecksStore> {
    return authenticatedRequest(`/checks/${checkId}`)
        .then(data => {
          const model = createInstance(CheckModel, data);
          model.orderedItems = this.mapResponseToModel(OrderedItemModel, model.orderedItems);
          return model;
        })
        .then(model => ({[model.id]: model}));
  }

  static addItemToCheck(checkId: string, itemId: string): Promise<OrderedItemModel> {
    return authenticatedRequest(`/checks/${checkId}/addItem`, 'PUT', { itemId });
  }

  static voidItemOnCheck(checkId: string, orderedItemId: string): Promise<OrderedItemModel> {
    return authenticatedRequest(`/checks/${checkId}/voidItem`, 'PUT', { orderedItemId });
  }

  static closeCheck(checkId: string): Promise<CheckModel> {
    return authenticatedRequest(`/checks/${checkId}/close`, 'PUT');
  }

  static deleteAllChecks(): Promise<void> {
    return authenticatedRequest('/checks', 'DELETE');
  }

  private static mapResponseToModel(model: any, data: any) {
    return data.map((d: any) => createInstance(model, d));
  }

  // use key value store
  private static mapModelsToStore(models: any) {
    return models.reduce(
        (map: any, model: any) => {
          map[model.id] = model;
          return map;
        },
        {}
    );
  }

}