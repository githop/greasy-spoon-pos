import * as React from 'react';
import './App.css';
import './styles.css';
import { BrowserRouter as Router, NavLink as Link, Switch, Route, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from './store';
import ChecksIndexContainer from './containers/ChecksIndexContainer';
import CheckDetailContainer from './containers/CheckDetailContainer';
import { AveroApiClient } from './lib/api-client';
import TablesIndexContainer from './containers/TablesIndexContainer';
const buttonClass = 'button button-black button-outline';
const activeStyle = {
  backgroundColor: 'black',
  color: 'white'
};
class App extends React.Component {

  deleteAll() {
    AveroApiClient.deleteAllChecks();
  }

  render() {
    return (
        <Provider store={store}>
          <Router>
            <div className="App">
              <nav className="nav">
                <div>
                  <Link activeStyle={activeStyle} className={buttonClass} to="/tables">Tables</Link>
                </div>
                <div>
                  <Link activeStyle={activeStyle} className={buttonClass} to="/checks">Checks</Link>
                </div>
              </nav>
              {/*<button className="button-black button-outline" onClick={() => this.deleteAll()}>reset</button>*/}

              <Switch>
                <Route exact={true} path="/" render={() => <Redirect to="/tables"/>}/>
                <Route exact={true} path="/tables" component={TablesIndexContainer}/>
                <Route exact={true} path="/checks" component={ChecksIndexContainer}/>
                <Route path="/checks/:id" component={CheckDetailContainer}/>
              </Switch>
            </div>
          </Router>
        </Provider>
    );
  }
}

export default App;
