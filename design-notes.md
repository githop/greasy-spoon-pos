
### Tables index
/tables
- list tables
- display if table has open check
- link to checks index, set selectedTable

### Checks index
/checks
combinations.
filter by:
- open
- closed
- table
- compose filters
- navigate to detail / edit view

use toggle for group by table. button set (open, closed, all) for filter

### Checks detail / edit view
checks/:id
- list ordered items (show void state, allow set void = true)
- list menu items
- add menu item
- void menu item
- close check